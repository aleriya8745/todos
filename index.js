$(document).ready(function () {
    const todoArr = [];

    $('#todo2').click(function () {
        enter();
    });

    $('#todo').keydown(function () {
        if (event.keyCode == 13) {
            enter();
        }
    });

    var count = 0;

    function enter() {
        var val = $('#todo').val().trim();

        if (val) {
            console.log('function start');
            $('#todo-list').append('<li id="' + count + '"><input type="checkbox" class="checkbox" /><label class="hidetext" for=".checkbox">' + val + '</label><input class="text" style="display: none" value="' + val + '"><input type="submit" class="delete" value="Delete"/></li>');
            $("#todo").val("");
            $("#ifNotTodo").hide();
            var obj = {
                text: val,
                checked: false,
                id: count
            };
            todoArr.push(obj);
            count++;
        }
        findNumber();

    }

    $("body").on("change", '.checkbox', function () {
        console.log('change', this.checked);

        if (this.checked) {
            $(this).parent().addClass('checked');
        }

        else {
            $(this).parent().removeClass('checked');
        }
        findNumber();

    });


    $("body").on("click", ".delete", function () {
        console.log('change', this.checked);
        $(this).parent().remove();
        findNumber();


    });
    $("body").on("change", '#allchecked', function () {
        console.log('change', this.checked);
        if (this.checked) {
            $('.checkbox').prop('checked', true);
            $('#todo-list li').addClass('checked');
        }
        else {
            $('.checkbox').prop('checked', false);
            $('#todo-list li').removeClass('checked');
        }
        findNumber();

    });

    $('#alldeleted').click(function () {
        console.log('cliiiick');

        $(".checked").remove();
        $("#allchecked").prop('checked', false);

        findNumber();

    });

    $("body").on("dblclick", ".hidetext", function () {

        var that = $(this);
        that.hide();
        that.next().show().focus();
        that.next().blur(function () {
            saveChange($(this))
        });

        that.next().keypress(function (event) {
            if (event.keyCode == 13) {
                saveChange($(this))
            }
        });

        function saveChange(input) {
            var newText = input.val();

            if ($.trim(newText)) {
                that.text(newText).show();
                input.hide();
            }
            else {
                return false
            }
        }

    });

    function countTodo() {
        var check = $("#todo-list").find("input[type=checkbox]:checked").length,
            noCheck = $("#todo-list").find("input[type=checkbox]:not(:checked)").length,
            allTodo = $("#todo-list li").length;
        $("#noactivecheck").text(check);
        $("#activecheck").text(noCheck);
        if (check && check == allTodo) {
            $('#allchecked').prop('checked', true);
        }
        else {
            $('#allchecked').prop('checked', false);
        }
    }

    $('.menu-tab').click(function () {
        $('.menu-tab').removeClass('active');
        $(this).addClass('active');
        showTodo($(this).attr('id'));

    });

    function showTodo() {
        findNumber()
    }

    const liPerPage = 5;


    function findNumber() {
        var activeTab = $('.menu-tab.active').attr('id');
        var allTodo = $("#todo-list li").length;

        if (activeTab == 'page-active') {
            allTodo = $("#todo-list li:not(.checked)").length;
        }
        else if (activeTab == 'page-completed') {
            allTodo = $("#todo-list li.checked").length;
        }

        var pagesNumber = Math.ceil(allTodo / liPerPage);

        var activePage = Number($('.page_number.active').attr('id'));

        renderPages(pagesNumber, activePage);
        showSpan();

    }


    function renderPages(pagesNumber, activePage) {

        if (!pagesNumber) {
            pagesNumber = 1;
        }
        $('#pages a').remove();
        for (var i = 1; i <= pagesNumber; i++) {

            var className = 'page_number';
            if (i == activePage) {
                className = className + ' active';
            }
            $('#pages').append("<a class='" + className + "' id='" + i + "'>" + i + "</a>")
        }

        if ($('.page_number.active').length == 0) {
            $('.page_number:last-child').addClass('active');
            activePage = pagesNumber;
        }
        showSlice(activePage);
    }

    $("body").on("click", ".page_number", function () {
        $(".page_number").removeClass('active');
        $(this).addClass('active');
        showSpan();
        showSlice($(this).attr('id'));
    });

    function showSlice(page) {
        var page = page || 1;
        var a = page * 5;
        var x = a - 5;
        var activeTab = $('.menu-tab.active').attr('id');

        $('#todo-list li').hide();

        if (activeTab == 'page-all') {
            $('#todo-list li').slice(x, a).show();
        }

        if (activeTab == 'page-active') {
            $("#todo-list li:not(.checked)").slice(x, a).show();
        }

        else if (activeTab == 'page-completed') {
            $("#todo-list li.checked").slice(x, a).show();
        }

        countTodo();

    }


    function showSpan() {
        var allTodo = $("#todo-list li").length;
        if ( allTodo == 0) {
            $("#ifNotTodo").show();

        }

    }

})